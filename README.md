**What?**
Generates neat patterns from scratch! 


**Why?**
I use this to make thumbnails for my tracks on SoundCloud, because I'm 1) Too lazy and 2) Not artistic enough to design a nice piece of art every time.


**How?**
Build from ./src/gen.js as ./build.js with webpack or something and open in browser.


**More?**
I will at some point turn this into a CLI to generate these things straight from the terminal. 
