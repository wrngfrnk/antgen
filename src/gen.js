const { createCanvas, loadImage } = require('canvas')
import background from './generators/background.js'
import theme from './generators/theme.js'
import path from './generators/path.js'

const debug = false

const canvasParams = {
  size: {
    x: 1000,
    y: 1000,
  },
  dim: {
    x: 5,
    y: 5
  },
  margin: {
    x: 200,
    y: 200
  }
}

// init
let settings, ant, seed, initSeed, colors, verts, canvas, ticker, prePath, ctx



// TODO: !! DOING !! Split into separate components
// TODO: Allow custom non-seeded parameters
// TODO: UI
// TODO: More ant algorithms
// TODO: ## DONE ## Multiple fill zones
// TODO: ## DONE ## Prevent leaving loose ends in the path
// TODO: ## DONE / WIP ## More background generators

const getSettings = () => {
  
}

const updateSettings = (settings) => {
  settings = settings
  // ... do something here
}

/* Generates seed */
const generateSeed = (inputSeed) => {
  let newSeed = inputSeed
  if(!newSeed) {
    newSeed = []
    for(let i = 0; i < 9; i++) {
      newSeed.push(Math.floor(Math.random() * 10))
    }
    newSeed = newSeed.join('')
  }

  initSeed = newSeed
  seed = newSeed
}

/* Generates Params from given seed */
const seededRandom = (min=0, max=1) => {
  seed = (seed * 9301 + 49297) % 233280
  let rnd = seed / 233280
  rnd = min + rnd * (max - min)
  return rnd
}

const getTheme = () => {
  let themes = theme.getThemes()
  colors = theme.load(themes[Math.round(seededRandom(0, themes.length-1))])
}

/* Creates and places vertices */
function Vertex(id, x, y, xPos, yPos) {
  // Represents the graphical locations of anchor points
  this.id = id
  this.x = x
  this.y = y
  this.xPos = xPos // Do verts need to know their canvas positions?
  this.yPos = yPos

  this.getAvailableNeighbours = () => {
    // Returns an array of available directions, indexed by keypad notation
    // so that up = 8, down = 2, down-left = 1, etc. 0 is ignored
    let sX = canvasParams.dim.x - 1
    let sY = canvasParams.dim.y - 1
    let u = this.y > 0,
        d = this.y < sY,
        l = this.x > 0,
        r = this.x < sX

    let dirs = [ 
      [u&&l,  u,      u&&r],
      [l,     false,  r   ],
      [d&&l,  d,      d&&r]
    ]

    let destinations = []
    for(let i = 0; i < 3; i++) {
      for(let j = 0; j < 3; j++) {
        let destX, destY
        if(dirs[i][j]) {
          destX = this.x + (j - 1)
          destY = this.y + (i - 1)
          destinations.push(getVertByXY(destX, destY))
        } else {
          destinations.push(false)
        }
      }
    }

    destinations = [
      false,
      ...destinations.slice(6, 9),
      ...destinations.slice(3, 6),
      ...destinations.slice(0, 3)
    ]

    return destinations
  }
}

const getVert = (id) => {
  let v = verts.reduce((a, b) => [...a, ...b], []) 
  return v.find(v => v.id == id)
}

const getVertByXY = (x, y) => {
  return verts[y][x]
}

const getVertInDir = (origin, dir) => {
  let n = origin.getAvailableNeighbours()
  return n[dir]
}

// Gets the direction in which to turn to reach target from id
const getDirToVert = (origin, target) => {
  let dir = 5 // 5 is neutral/no direction
  let h = 0, // 0 = same axis
      v = 0

  // since the "up" directions are all in the range 7 to 9, 
  // we can simply add 3 to the neutral direction to go up,
  // and vice versa remove 3 to go down. Left/right work the
  // same, but with + or - 1.
  if(origin.x !== target.x) h = origin.x > target.x ? -1 : 1
  if(origin.y !== target.y) v = origin.y > target.y ? 3 : -3
  
  dir = dir + h + v
  return dir
}

const getOriginVert = () => {
  return getVert(Math.round(seededRandom(0, canvasParams.dim.x * canvasParams.dim.y - 1)))
}

const generateVerts = () => {
  let marX = canvasParams.margin.x
  let marY = canvasParams.margin.y

  let bndX = canvasParams.size.x - (marX * 2) // Boundaries, maximum drawable area
  let bndY = canvasParams.size.y - (marY * 2)
  
  let nX = canvasParams.dim.x
  let nY = canvasParams.dim.y

  let deltaX = bndX / (nX - 1) // distance between verts
  let deltaY = bndY / (nY - 1) 
  
  let newVerts = []

  for(let i = 0; i < nY; i++) {
    let xVerts = []
    let yCoord = marY + (deltaY * i)
    let newVert

    for(let j = 0; j < nX; j++) {
      let id = (nX * i) + j
      let xCoord = marX + (deltaX * j)
  
      newVert = new Vertex(id, j, i, xCoord, yCoord)
      xVerts.push(newVert)
    }
    
    newVerts.push(xVerts)
  } 

  verts = newVerts
}

/* Ant, the pathfinder */
// Travels between verts
function Ant(origin, initDir) {
  this.origin = origin
  this.dir = initDir
  this.path = [origin]
  this.alive = true
  this.pos = origin

  let nSteps = 0
  let lifetime = canvasParams.dim.x * canvasParams.dim.y
  let minimumSteps = 6
  let stepsSinceTurn = 0
  let stepsPerTurn = Math.round((canvasParams.dim.x + canvasParams.dim.y)) * 1.5

  this.step = () => {
    let prev = this.path[nSteps-2]
    let next = getVertInDir(this.pos, this.dir)
    let shouldTurn = stepsSinceTurn >= stepsPerTurn || nSteps > lifetime
    let available

    if(shouldTurn) {
      stepsSinceTurn = 0
      let nextDir = getDirToVert(this.pos, this.origin)
      next = getVertInDir(this.pos, nextDir)
      
      if(next == prev) {
        next = false
        stepsSinceTurn = stepsPerTurn - 1
      }
    }

    if(!next) { 
      let neighbours = this.pos.getAvailableNeighbours()
      available = neighbours.filter(v => v && v !== prev)
      next = available[Math.round(seededRandom(0, available.length-1))]
    }

    stepsSinceTurn += 1

    this.dir = getDirToVert(this.pos, next)
    this.pos = next
    this.path.push(this.pos)
    nSteps = this.path.length

    if(checkShouldDie()) this.kill()
    return next
  }

  this.kill = () => {
    this.alive = false
  }

  // Performs some checks to decide if the ant is done, i.e.
  // if it has drawn an "interesting" enough pattern.
  const checkShouldDie = () => {
    // has it returned to the origin?
    let isAtOrigin = this.origin == this.pos 

    // is the return vector to the origin the same as the departure vector?
    let originVectorOverlaps = (this.path[1] == this.path[nSteps-2])

    // has it moved enough steps or is its lifetime over?
    let lifespanOver = minimumSteps > nSteps > lifetime

    // is there an intersection at the origin vector, i.e. is the path closed?
    let originIntersects = this.path.slice(1).includes(this.origin)

    return ((lifespanOver && originIntersects) || isAtOrigin) && !originVectorOverlaps
  }
}

const update = () => {
  ant.step();

  let antPos = {
    x0: ant.pos.xPos,
    y0: ant.pos.yPos,
    x1: ant.path[ant.path.length-2].xPos,
    y1: ant.path[ant.path.length-2].yPos
  }

  ctx.beginPath()
  ctx.strokeStyle = colors.line[0]
  ctx.lineWidth = 2
  ctx.lineCap = "round"
  ctx.moveTo(antPos.x0, antPos.y0)
  ctx.lineTo(antPos.x1, antPos.y1)
  ctx.stroke()
}

const tick = () => {
  if(ant.alive) update()
  if(!ant.alive) {
    clearInterval(ticker)
    drawPath()
  }
}

const refitCanvas = () => {
  let canvasArea = document.getElementById("app")
  let canvasParent = document.getElementById("main")
  let canvasZoomX = canvasParent.offsetWidth / canvasParams.size.x
  let canvasZoomY = canvasParent.offsetHeight / canvasParams.size.y
  let zoom = Math.min(canvasZoomX, canvasZoomY)

  if(zoom > 1) canvasZoom = 1
  canvasArea.style.transform = "scale(" + zoom + ")"
 
}

const generateBg = (ctx) => {
  let type = background.types
  type = type[Math.round(seededRandom(0, background.types.length-1))]
  background.generate(ctx, canvasParams.size.x, canvasParams.size.y, type, colors.bg)
}

const drawPath = () => {
  let pathProps = {
    colors: colors.line,
    width: 12 // (canvasParams.size.x + canvasParams.size.y / 2) / 100
  }

  path.draw(ctx, ant.path, pathProps)
}

const init = (inputSeed, antProps = {}) => {
  // Reset the ticker
  clearInterval(ticker)

  // Generate a new seed
  generateSeed(inputSeed)

  // TODO: MOVE THIS updates the seed text output
  document.getElementById("seed-text").innerText = seed

  // Generates and places the verticles
  generateVerts()

  // Select a theme
  getTheme()

  // Generate the background
  generateBg(ctx)

  // Resize the canvas to fit the screen
  refitCanvas()

  antProps = {
    origin:  antProps.origin ? getVert(antProps.origin.x, antProps.origin.y) : getOriginVert(),
    initDir: antProps.initDir || Math.round(seededRandom(1, 9)),
    path:    antProps.path || false,
  }

  ant = new Ant(antProps.origin, antProps.initDir, antProps.path)

  ticker = setInterval(tick, 1000 / 60)
}

document.addEventListener("DOMContentLoaded", function(event) {
  canvas = createCanvas(canvasParams.size.x, canvasParams.size.y)
  ctx = canvas.getContext('2d')

  document.getElementById("app").appendChild(canvas)

  let goBtn = document.getElementById("generate")
  let raveBtn = document.getElementById("rave")
  let bpmBox = document.getElementById("bpm")
  let tapBtn = document.getElementById("bpm-tap")

  let inputBox = document.getElementById("seed")
  let seedBox = document.getElementById("seed-text")
  seedBox.innerText = initSeed

  goBtn.onclick = () => {
    let inputSeed = inputBox.value || undefined
    init(inputSeed)
  }

  let bpm = bpmBox.value
  let rave = false
  let tempo
  let taps = []
  let lastTap = 0

  tapBtn.onclick = () => {
    timeStamp = Math.floor(Date.now())
    if(taps.length >= 5) taps.shift()
    let timeSinceLastTap = timeStamp - lastTap

    if(timeSinceLastTap < 2000) {
      taps.push(timeSinceLastTap)
      bpm = Math.round(60000 / (taps.reduce((a, v) => a + v) / taps.length))
    } else {
      taps = []
    }

    lastTap = timeStamp
    bpmBox.value = bpm
  }

  raveBtn.onclick = () => {
    rave = !rave
    if(rave) tempo = setInterval(() => {
      init()
    }, 60000 / bpm)
    if(!rave) clearInterval(tempo)
  }

  window.onresize = () => refitCanvas()
  init(false, {
    origin: { x: 0, y: 4 },
    initDir: 8,
    path: []
  })
})



