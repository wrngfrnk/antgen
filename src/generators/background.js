import * as bg from './backgrounds/index.js'
// TODO: Make this accept some kind of {props}-like object

const bgTypes = { // perhaps I can generate this automatically
  'spotlight': bg.spotlight,
  'stripes': bg.stripes,
  'spiral': bg.spiral,
  'triangles': bg.triangles,
  'gradient': bg.gradient,
  'frame': bg.frame,
  'dot': bg.dot,
  'hexagons': bg.hexagons,
  'grid': bg.grid,
  'checker':  bg.checker,
  'hypno': bg.hypno,
  'vinyl': bg.vinyl,
  'sun': bg.sun,
  // 'glass'
  // 'polkadot',
  // ''
}

const overlayTypes = [
  'round',
  'quadratic',
  'vgradient',
  'hgradient',
  'dgradient',
]

const types = Object.keys(bgTypes)

const overlay = () => {
  // draw an overlay on the background to give it some variation, or something
}

const generate = (ctx, x, y, type, colors, overlay = false) => {
  // if(debug) {
  //   ctx.fillStyle = 'white';
  //   verts.forEach((vx, i) => {
  //     vx.forEach((vy, j) => {
  //       ctx.beginPath();
  //       ctx.arc(vy.xPos, vy.yPos, 2, 0, Math.PI * 2, false);
  //       ctx.fill();
  //       ctx.font = "30px Monospace";
  //       ctx.fillText(vy.id, vy.xPos, vy.yPos)
  //     })
  //   })
  // }

  let bgType = bgTypes[type]
  
  ctx.beginPath()
  ctx.rect(0, 0, x, y)
  ctx.fillStyle = colors[0]
  ctx.fill()

  bgType(ctx, x, y, colors)
  // if(overlay) overlay()
}


export default { generate, types }