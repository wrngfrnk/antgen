const stripes = (ctx, sX, sY, colors) => { 
  ctx.beginPath()
  ctx.moveTo(sX/2, sY/2)

  let angle, x, y
  for (let i = 0; i < Math.max(sY, sX)*7.2; i++) {
    angle = 0.1 * i;
    x = sX/2 + (1 + angle) * Math.cos(angle)
    y = sY/2 + (1 + angle) * Math.sin(angle)
    ctx.lineTo(x, y)
  }

  ctx.lineWidth = 4
  ctx.strokeStyle = colors[1]
  ctx.stroke()
}

export default stripes