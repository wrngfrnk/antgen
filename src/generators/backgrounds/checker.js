const checker = (ctx, sX, sY, colors) => {
  let size = 120
  ctx.fillStyle = colors[1]
  for(let x = 0; x < Math.ceil(sX/size); x++) {
    for(var y = 0; y < Math.ceil(sY/size); y++) {
      if((x % 2 == 1 && y % 2 == 0) || (x % 2 == 0 && y % 2 == 1)) {
        ctx.fillRect(x*size, y*size, size, size)
      } 
    }
  }
}

export default checker