const triangles = (ctx, sX, sY, colors) => {
  let triSize = Math.min(sX, sY) / 20
  ctx.beginPath()
  for(let y = 0; y < sY/triSize; y++) {
    ctx.moveTo(0, y*triSize)
    for(let x = 0; x < sX/triSize; x++) {
      ctx.lineTo(x*triSize, y*triSize + triSize)
      ctx.lineTo(x*triSize+triSize, y*triSize) 
    }
  }
  
  ctx.fillStyle = colors[1]
  ctx.fill()
}

export default triangles