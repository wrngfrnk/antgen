const sun = (ctx, sX, sY, colors) => {
  let nBeams = 8
  let offsetX = sX * 0
  let offsetY = sY * 0
  let x = -offsetX
  let y = sY + offsetY

  ctx.fillStyle = colors[1]

  const rad = (deg) => {
    return (Math.PI / 180) * deg
  }

  for(let i = 0; i < nBeams; i+=2) {
    let angle1 = rad((i*(60/(nBeams-1)))*2) // wtf
    let angle2 = rad(((i+1)*(60/(nBeams-1)))*2) 

    let nX1 = (Math.cos(angle1) * sX) + (Math.sin(angle1) * sY)
    let nY1 = (Math.cos(angle1) * sY) - (Math.sin(angle1) * sX)
    let nX2 = (Math.cos(angle2) * sX) + (Math.sin(angle2) * sY)
    let nY2 = (Math.cos(angle2) * sY) - (Math.sin(angle2) * sX)

    ctx.beginPath()
    ctx.moveTo(nX1, nY1)
    ctx.lineTo(x, y)
    ctx.lineTo(nX2, nY2)
    ctx.fill()
    ctx.closePath()
  }

  ctx.strokeStyle = colors[0]
  ctx.lineWidth = 40
  ctx.beginPath()
  ctx.moveTo(0, 0)
  ctx.lineTo(sX, 0)
  ctx.lineTo(sX, sY)
  ctx.lineTo(0, sY)
  ctx.lineTo(0, 0)
  ctx.stroke()
}

export default sun