const drawShadow = (ctx, path, props) => {
  ctx.lineWidth = props.width
  ctx.filter = 'blur(' + props.blur + 'px)'
  // let shadowOffset = 6
  let shadowPath = new Path2D(path)
  ctx.strokeStyle = 'rgba(0,0,0,0.15)' // TODO: Other shadow colors?
  // ctx.translate(shadowOffset, shadowOffset)
  ctx.stroke(shadowPath)
  // ctx.translate(-shadowOffset, -shadowOffset)
  ctx.filter = 'blur(0px)'
}

const strokePath = (ctx, path, props) => {
  let _path = new Path2D(path)
  ctx.lineWidth = props.width
  ctx.translate(props.offset, props.offset)
  ctx.strokeStyle = props.color
  ctx.stroke(_path)
  ctx.translate(-props.offset, -props.offset)
}

// Find the indices of the first occurring duplicate (first value to occur twice)
const findIntersect = (path) => {
  let intersects = []
  let array = path.map(v => v.id)
  let o = array.reduce((acc, v) => {
    acc[v] = 0
    return acc
  }, {})

  for(let i=0; i<array.length; i++) {
    o[array[i]] += 1
    if(o[array[i]] > 1) {
      intersects.push([array.findIndex(v => v == array[i]), i])
    }
  }

  return intersects
}

const fillIntersects = (ctx, path, props) => {
  let intersects = findIntersect(path).sort((a, b) => {
    return (a[0] - b[0]) + (b[1] - a[1])
  })

  if(intersects.length > 1) {
    intersects = intersects.filter(v => {
      return v[0] != 0 && v[1] != path.length-1
    })
  }

  let intersectPaths = []
  intersects.forEach((x, i) => {
    let fillVerts = path.slice(x[0], x[1])
    let fillPath = new Path2D()
    fillPath.moveTo(fillVerts[0].xPos, fillVerts[0].yPos)
    
    fillVerts.forEach(v => {
      fillPath.lineTo(v.xPos, v.yPos)
    })

    intersectPaths.push(fillPath)
  })

  intersectPaths.forEach((v, i) => {
    ctx.fillStyle = props.colors[i % 2]
    ctx.fill(v)
  })
}

const draw = (ctx, path, props = {}) => {
  props = {
    main: {
      color:  props.colors[0] || '#ffffff',
      width: props.width || 10
    }, 
    shade: {
      color: props.colors[1] || '#c2524e',
      width: props.width || 10,
      offset: (props.width / 2) - 1
    },
    shadow: {
      width: props.width + 5,
      blur: 12,
    }, 
    fill: {
      colors: props.colors
    }
  }

  let path2d = new Path2D()
  let origin = path[0]

  // ctx.beginPath()
  ctx.lineCap = "round"
  ctx.lineJoin = "round"
  ctx.fillStyle = "white"

  path2d.moveTo(origin.xPos, origin.yPos)
  path.forEach((v, i) => {
    path2d.lineTo(v.xPos, v.yPos)
  })

  drawShadow(ctx, path2d, props.shadow)
  strokePath(ctx, path2d, props.shade)
  fillIntersects(ctx, path, props.fill)
  strokePath(ctx, path2d, props.main)
  // ctx.lineWidth = props.width
  // ctx.strokeStyle = props.colors[0]
  // ctx.stroke(mainPath)

  // let buf = canvas.toBuffer()
  // fs.writeFileSync('output/' + seed + '.png', buf)
  // fs.writeFileSync('test.png', buf)
}

export default { draw }